INSTALLATION
####
run
```
docker-compose up -d && docker-compose exec composer install
```

TESTING
####
for run tests execute: 
```
docker-compose exec backend vendor/bin/codecept run api
```