<?php

use Codeception\Util\HttpCode;

/**
 * Class CurrencyCest
 */
class CurrencyCest
{
    /**
     * @param ApiTester $I
     */
    public function testList(ApiTester $I)
    {
        $I->sendGet('/currencies');
        $I->canSeeResponseCodeIs(HttpCode::OK);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['currencies' => []]);
    }

    /**
     * @param ApiTester $I
     */
    public function testAdd(ApiTester $I)
    {
        $I->sendPost('/currencies');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        $I->sendPost('/currencies', ['ticker' => 'USD']);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'currency' => [
                'ticker' => 'USD'
            ]
        ]);

        $I->sendGet('/currencies');
        $I->seeResponseContainsJson([
            'currencies' => [
                ['ticker' => 'USD']
            ]
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function testRemove(ApiTester $I)
    {
        $I->sendDelete('/currencies');
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED);

        $I->addCurrency('USD');

        $I->sendDelete('/currencies/USD');
        $I->seeResponseCodeIs(HttpCode::OK);

        $I->sendGet('/currencies');
        $I->seeResponseContainsJson([
            'currencies' => []
        ]);

        $I->sendDelete('/currencies/USD', ['ticker' => 'USD']);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }
}
