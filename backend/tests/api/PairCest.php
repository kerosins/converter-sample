<?php

use Codeception\Example;
use Codeception\Util\HttpCode;

class PairCest
{
    public function testList(ApiTester $I)
    {
        $I->addCurrency('USD');
        $I->addCurrency('EUR');
        $I->addCurrency('RUR');

        $I->addPair('USD', 'EUR', 0.7);
        $I->addPair('USD', 'RUR', 75);
        $I->addPair('EUR', 'RUR', 80);

        $I->sendGet('pairs');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'pairs' => [
                'USD' => [
                    [
                        'from' => 'USD',
                        'to' => 'USD',
                        'rate' => 1
                    ],
                    [
                        'from' => 'USD',
                        'to' => 'EUR',
                        'rate' => 0.7
                    ],
                    [
                        'from' => 'USD',
                        'to' => 'RUR',
                        'rate' => 75
                    ],
                ],
                'RUR' => [
                    [
                        'from' => 'RUR',
                        'to' => 'RUR',
                        'rate' => 1
                    ],
                    [
                        'from' => 'RUR',
                        'to' => 'USD',
                        'rate' => 0.01333
                    ],
                    [
                        'from' => 'RUR',
                        'to' => 'EUR',
                        'rate' => 0.0125
                    ],
                ],
                'EUR' => [
                    [
                        'from' => 'EUR',
                        'to' => 'EUR',
                        'rate' => 1
                    ],
                    [
                        'from' => 'EUR',
                        'to' => 'USD',
                        'rate' => 1.42857
                    ],
                    [
                        'from' => 'EUR',
                        'to' => 'RUR',
                        'rate' => 80
                    ],
                ]
            ]
        ]);
    }

    /**
     * @param ApiTester $I
     *
     * @example [0.7, 1.42857]
     * @example [1.3, 0.76923]
     * @example [70, 0.01428]
     */
    public function testAdd(ApiTester $I, Example $example)
    {
        $rate = $example[0];
        $expectedReverseRate = $example[1];

        $I->addCurrency('CUR1');
        $I->addCurrency('CUR2');

        $I->sendPost('/pairs/CUR1/CUR2', ['rate' => $rate]);
        $I->seeResponseCodeIs(HttpCode::OK);

        $I->seeResponseContainsJson([
            'from' => 'CUR1',
            'to' => 'CUR2',
            'rate' => $rate
        ]);

        $I->sendGet('/pairs/CUR2/CUR1');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'from' => 'CUR2',
            'to' => 'CUR1',
            'rate' => $expectedReverseRate
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function testAddPairForNonExistingCurrency(ApiTester $I)
    {
        $I->sendPost('/pairs/TEST/NON-EXISTING');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        $I->addCurrency('CUR1');
        $I->addCurrency('CUR2');
        $I->sendPost('/pairs/CUR1/CUR2');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }

    public function testRemove(ApiTester $I)
    {
        $I->addCurrency('CUR1');
        $I->addCurrency('CUR2');
        $I->addPair('CUR1', 'CUR2', 0.6);

        $I->sendDelete('/pairs/CUR1/CUR2');
        $I->seeResponseCodeIs(HttpCode::OK);

        $I->sendGet('/pairs/CUR1/CUR2');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);

        $I->sendGet('/pairs/CUR2/CUR1');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }
}
