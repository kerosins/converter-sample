<?php

use Codeception\Example;
use Codeception\Util\HttpCode;

class RateCest
{

    /**
     * @param ApiTester $I
     * @param Example $example
     *
     * @example [0.7, 100, 70]
     * @example [80, 100, 8000]
     * @example [0.0145, 100, 1.45]
     */
    public function testExec(ApiTester $I, Example $example)
    {
        $I->addCurrency('CUR1');
        $I->addCurrency('CUR2');
        $I->addPair('CUR1', 'CUR2', $example[0]);

        $I->sendGet('/rate/CUR1/CUR2/' . $example[1]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'from' => 'CUR1',
            'to' => 'CUR2',
            'value' => $example[2]
        ]);
    }
}
