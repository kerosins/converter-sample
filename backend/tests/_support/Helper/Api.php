<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Codeception\Module;
use Codeception\TestInterface;
use Converter\App;
use Predis\Client;

class Api extends Module
{
    /**
     * @var App Application
     */
    private App $app;

    public function _initialize()
    {
        $this->app = new App();
        $this->app->enableTestEnvironment();
        $this->app->initDi();
    }

    public function _before(TestInterface $test)
    {
        /** @var Client $redis */
        $redis = $this->app->getContainer()->get('redis');
        $redis->flushdb();
    }
}
