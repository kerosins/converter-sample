<?php

use Codeception\Actor;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends Actor
{
    use _generated\ApiTesterActions;

    /**
     * Define custom actions here
     */

    /**
     * Add currency
     *
     * @param string $ticker
     */
    public function addCurrency(string $ticker)
    {
        $this->sendPost('/currencies', ['ticker' => $ticker]);
    }

    /**
     * Add pair through api
     *
     * @param string $tickerFrom
     * @param string $tickerTo
     * @param float $rate
     */
    public function addPair(string $tickerFrom, string $tickerTo, float $rate)
    {
        $this->sendPost('/pairs/' . $tickerFrom . '/' . $tickerTo, ['rate' => $rate]);
    }
}
