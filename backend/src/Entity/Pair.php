<?php

namespace Converter\Entity;

use JsonSerializable;

class Pair implements JsonSerializable
{
    /**
     * @var Currency
     */
    private Currency $from;

    /**
     * @var Currency
     */
    private Currency $to;

    /**
     * @var float
     */
    private float $rate;

    /**
     * Pair constructor.
     * @param Currency $from
     * @param Currency $to
     * @param float $scale
     */
    public function __construct(Currency $from, Currency $to, float $scale = 1.0)
    {
        $this->from = $from;
        $this->to = $to;
        $this->rate = $scale;
    }

    /**
     * @return Currency
     */
    public function getFrom(): Currency
    {
        return $this->from;
    }

    /**
     * @return Currency
     */
    public function getTo(): Currency
    {
        return $this->to;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate(float $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'from' => $this->from->getTicker(),
            'to' => $this->to->getTicker(),
            'rate' => $this->rate
        ];
    }
}
