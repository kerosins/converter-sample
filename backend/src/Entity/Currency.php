<?php

namespace Converter\Entity;

use JsonSerializable;

class Currency implements JsonSerializable
{
    /**
     * Currency's ticker
     *
     * @var string
     */
    private string $ticker;

    /**
     * Currency constructor.
     * @param string $ticker
     */
    public function __construct(string $ticker)
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'ticker' => $this->getTicker()
        ];
    }
}
