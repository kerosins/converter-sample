<?php

namespace Converter\Controller;

use Converter\Entity\Currency;
use Converter\Entity\Pair;
use Converter\Exception\EntityNotFoundException;
use Converter\Repository\PairRepository;
use Converter\Service\PairManager;
use League\Route\Http\Exception\BadRequestException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class PairController
 * @package Converter\Controller
 */
class PairController
{
    /**
     * @var PairRepository
     */
    private PairRepository $repository;
    /**
     * @var PairManager
     */
    private PairManager $pairManager;

    /**
     * PairController constructor.
     * @param PairRepository $repository
     * @param PairManager $pairManager
     */
    public function __construct(PairRepository $repository, PairManager $pairManager)
    {
        $this->repository = $repository;
        $this->pairManager = $pairManager;
    }

    /**
     * Returns list of all currency's pairs
     *
     * @return array
     */
    public function list()
    {
        return ['pairs' => $this->repository->getAll()];
    }

    /**
     * Returns list of pairs for concrete currency
     *
     * @param ServerRequestInterface $request
     * @param array $args
     * @return array
     */
    public function get(ServerRequestInterface $request, array $args)
    {
        return [
            'pairs' => $this->repository->getAllByCurrency(new Currency($args['currency']))
        ];
    }

    /**
     * Returns concrete pair
     *
     * @param ServerRequestInterface $request
     * @param array $args
     * @return Pair
     * @throws EntityNotFoundException
     */
    public function getConcrete(ServerRequestInterface $request, array $args): Pair
    {
        return $this->repository->get(
            new Currency($args['from']),
            new Currency($args['to'])
        );
    }

    /**
     * adds new pair
     *
     * @param ServerRequestInterface $request
     * @param array $args
     * @return Pair
     * @throws BadRequestException
     */
    public function add(ServerRequestInterface $request, array $args)
    {
        $rate = $request->getParsedBody()['rate'] ?? null;
        if (!$rate) {
            throw new BadRequestException('Parameter "rate" is required');
        }

        $pair = new Pair(
            new Currency($args['from']),
            new Currency($args['to']),
            (float) $rate
        );
        try {
            $this->pairManager->addPair($pair);
        } catch (EntityNotFoundException $e) {
            throw new BadRequestException($e->getMessage());
        }

        return $pair;
    }

    /**
     * removes pair
     *
     * @param ServerRequestInterface $request
     * @param array $args
     * @return bool[]
     * @throws BadRequestException
     */
    public function remove(ServerRequestInterface $request, array $args)
    {
        try {
            $this->pairManager->removePair(
                new Pair(
                    new Currency($args['from']),
                    new Currency($args['to'])
                ),
            );
        } catch (EntityNotFoundException $e) {
            throw new BadRequestException($e->getMessage());
        }

        return ['success' => true];
    }
}
