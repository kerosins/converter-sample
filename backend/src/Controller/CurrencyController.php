<?php

namespace Converter\Controller;

use Converter\Entity\Currency;
use Converter\Repository\CurrencyRepository;
use Converter\Service\PairManager;
use League\Route\Http\Exception\BadRequestException;
use Psr\Http\Message\ServerRequestInterface;

class CurrencyController
{
    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;
    /**
     * @var PairManager
     */
    private PairManager $pairManager;

    /**
     * CurrencyController constructor.
     * @param CurrencyRepository $currencyRepository
     * @param PairManager $pairManager
     */
    public function __construct(CurrencyRepository $currencyRepository, PairManager $pairManager)
    {
        $this->currencyRepository = $currencyRepository;
        $this->pairManager = $pairManager;
    }

    /**
     * @return array
     */
    public function index()
    {
        return [
            'currencies' => $this->currencyRepository->getAll()
        ];
    }

    /**
     * @param ServerRequestInterface $request
     * @return Currency[]
     * @throws BadRequestException
     */
    public function add(ServerRequestInterface $request)
    {
        $ticker = $request->getParsedBody()['ticker'] ?? null;

        $currency = new Currency($ticker);
        $this->currencyRepository->add($currency);

        return [
            'currency' => $currency
        ];
    }

    /**
     * @param ServerRequestInterface $request
     * @param array $args
     * @return array
     * @throws BadRequestException
     */
    public function remove(ServerRequestInterface $request, array $args)
    {
        $ticker = $args['ticker'] ?? null;
        if (!$ticker) {
            throw new BadRequestException('Parameter "ticker" is required');
        }

        $currency = new Currency($ticker);

        $this->pairManager->removeAllPairsByCurrency($currency);
        $this->currencyRepository->remove($currency);

        return [
            'success' => true
        ];
    }
}
