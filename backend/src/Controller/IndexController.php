<?php

namespace Converter\Controller;

class IndexController
{
    public function index()
    {
        return [
            'description' => 'sample converter',
            'version' => '1.0'
        ];
    }
}
