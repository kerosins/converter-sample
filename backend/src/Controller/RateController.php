<?php

namespace Converter\Controller;

use Converter\Entity\Currency;
use Converter\Service\Converter;
use Psr\Http\Message\ServerRequestInterface;

class RateController
{
    /**
     * @var Converter
     */
    private Converter $converter;

    /**
     * RateController constructor.
     * @param Converter $converter
     */
    public function __construct(Converter $converter)
    {
        $this->converter = $converter;
    }

    public function exec(ServerRequestInterface $request, array $args)
    {
        $from = new Currency($args['from']);
        $to = new Currency($args['to']);
        $amount = (float) $args['value'];

        return [
            'from' => $from->getTicker(),
            'to' => $to->getTicker(),
            'value' => $this->converter->convert($from, $to, $amount)
        ];
    }
}
