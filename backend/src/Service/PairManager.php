<?php

namespace Converter\Service;

use Converter\Entity\Currency;
use Converter\Entity\Pair;
use Converter\Repository\PairRepository;

class PairManager
{
    private const SCALE = 5;

    /**
     * @var PairRepository
     */
    private PairRepository $pairRepository;

    /**
     * PairManager constructor.
     * @param PairRepository $pairRepository
     */
    public function __construct(
        PairRepository $pairRepository
    ) {
        $this->pairRepository = $pairRepository;
    }

    /**
     * @param Pair $pair
     */
    public function addPair(Pair $pair): void
    {
        $this->pairRepository->save($pair);
        $reverseRate = bcdiv(1, $pair->getRate(), self::SCALE);

        $this->pairRepository->save(
            new Pair($pair->getTo(), $pair->getFrom(), $reverseRate)
        );
    }

    /**
     * @param Pair $pair
     */
    public function removePair(Pair $pair): void
    {
        $this->pairRepository->remove($pair);
        if ($pair->getFrom()->getTicker() !== $pair->getTo()->getTicker()) {
            $this->pairRepository->remove(
                new Pair($pair->getTo(), $pair->getFrom())
            );
        }
    }

    /**
     * @param Currency $currency
     */
    public function removeAllPairsByCurrency(Currency $currency): void
    {
        $pairs = $this->pairRepository->getAllByCurrency($currency);

        foreach ($pairs as $pair) {
            $this->removePair($pair);
        }
    }
}
