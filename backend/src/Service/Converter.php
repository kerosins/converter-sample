<?php

namespace Converter\Service;

use Converter\Entity\Currency;
use Converter\Repository\PairRepository;

class Converter
{
    /**
     * @var PairRepository
     */
    private PairRepository $repository;
    private int $precision;

    /**
     * Converter constructor.
     * @param PairRepository $repository
     * @param int $precision
     */
    public function __construct(PairRepository $repository, int $precision)
    {
        $this->repository = $repository;
        $this->precision = $precision;
    }

    /**
     * Converts from one currency to another
     *
     * @param Currency $from
     * @param Currency $to
     * @param float $amount
     * @return float
     * @throws \Converter\Exception\EntityNotFoundException
     */
    public function convert(Currency $from, Currency $to, float $amount): float
    {
        $pair = $this->repository->get($from, $to);
        return bcmul($amount, $pair->getRate(), $this->precision);
    }
}
