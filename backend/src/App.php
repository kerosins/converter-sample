<?php

namespace Converter;

use Converter\Router\Middleware\JsonRequestMiddleware;
use Converter\Router\Middleware\ValidationMiddleware;
use Converter\Router\RouteLoader;
use Converter\Router\Strategy\AppStrategy;
use Laminas\Diactoros\ResponseFactory;
use Converter\Router\Router;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Validator\ContainerConstraintValidatorFactory;
use Symfony\Component\Validator\ValidatorBuilder;

class App
{
    private bool $isTestEnvironment = false;

    /**
     * @var Container|ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * Enables test environment
     */
    public function enableTestEnvironment()
    {
        $this->isTestEnvironment = true;
    }

    /**
     * Process PSR-7 request
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function processRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->initDi();
        $this->initValidator();
        $this->configureRouter();

        return $this->container->get(Router::class)->handle($request);
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Initialize DI container
     *
     * @throws \Exception
     */
    public function initDi(): void
    {
        $containerBuilder = new ContainerBuilder();
        $loader = new YamlFileLoader(
            $containerBuilder,
            new FileLocator(dirname(__DIR__) . '/config')
        );
        $loader->load('parameters.yml');
        if ($this->isTestEnvironment) {
            $loader->load('parameters_test.yml');
        }

        $loader->load('services.yml');
        $containerBuilder->setParameter('root_dir', dirname(__DIR__));
        $containerBuilder->compile();

        $this->container = $containerBuilder;
    }

    /**
     * Initialize router and fill out routes list
     */
    private function configureRouter(): void
    {
        $responseFactory = new ResponseFactory();
        $strategy = new AppStrategy($responseFactory);
        $strategy->setValidator($this->container->get('validator'));
        $strategy->setContainer($this->container);

        /** @var Router $router */
        $router = $this->container->get(Router::class);
        $router->setStrategy($strategy);
        $router
            ->middleware(new JsonRequestMiddleware());

        /** @var RouteLoader $routeLoader */
        $routeLoader = $this->container->get(RouteLoader::class);
        $routeLoader->load();
    }

    /**
     * initializes validator
     */
    private function initValidator()
    {
        $validatorBuilder = new ValidatorBuilder();
        $validatorBuilder->setConstraintValidatorFactory(
            new ContainerConstraintValidatorFactory($this->container)
        );

        $this->container->set('validator', $validatorBuilder->getValidator());
    }
}
