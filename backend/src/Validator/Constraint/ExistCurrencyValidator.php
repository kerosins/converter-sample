<?php

namespace Converter\Validator\Constraint;

use Converter\Entity\Currency;
use Converter\Repository\CurrencyRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExistCurrencyValidator extends ConstraintValidator
{
    private CurrencyRepository $repository;

    /**
     * ExistCurrencyValidator constructor.
     * @param CurrencyRepository $repository
     */
    public function __construct(CurrencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$this->repository->has(new Currency($value))) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
