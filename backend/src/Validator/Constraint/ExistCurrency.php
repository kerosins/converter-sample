<?php

namespace Converter\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

class ExistCurrency extends Constraint
{
    public $message = 'Currency "{{ value }}" does not exist';
}
