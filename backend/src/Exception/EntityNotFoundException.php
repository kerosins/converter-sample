<?php

namespace Converter\Exception;

use League\Route\Http\Exception\NotFoundException;

class EntityNotFoundException extends NotFoundException
{
}