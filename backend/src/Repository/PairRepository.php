<?php

namespace Converter\Repository;

use Converter\Entity\Currency;
use Converter\Entity\Pair;
use Converter\Exception\EntityNotFoundException;
use Predis\Client;

class PairRepository
{
    /**
     * @var Client
     */
    private Client $redis;
    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;

    /**
     * PairRepository constructor.
     * @param Client $redis
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(Client $redis, CurrencyRepository $currencyRepository)
    {
        $this->redis = $redis;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Get list of all available pairs indexed by currency's ticker
     *
     * @return Pair[][]
     */
    public function getAll(): array
    {
        $currencies = $this->currencyRepository->getAll();
        $result = [];
        foreach ($currencies as $currency) {
            $result[$currency->getTicker()] = $this->getAllByCurrency($currency);
        }

        return $result;
    }

    /**
     * @param Currency $currency
     * @return Pair[]
     */
    public function getAllByCurrency(Currency $currency): array
    {
        $pairs = $this->redis->hgetall($currency->getTicker());
        return array_map(
            fn (string $ticker, float $rate) => new Pair(
                $currency,
                new Currency($ticker),
                $rate
            ),
            array_keys($pairs), $pairs
        );
    }

    /**
     * @param Currency $from
     * @param Currency $to
     * @return Pair
     * @throws EntityNotFoundException
     */
    public function get(Currency $from, Currency $to): Pair
    {
        $this->hasCurrencies($from, $to);

        $scale = $this->redis->hget($from->getTicker(), $to->getTicker());

        if (!$scale) {
            throw new EntityNotFoundException('Pair not found');
        }

        return new Pair($from, $to, $scale);
    }

    /**
     * @param Pair $pair
     * @throws EntityNotFoundException
     */
    public function save(Pair $pair): void
    {
        $this->hasCurrencies($pair->getFrom(), $pair->getTo());

        $this->redis->hset(
            $pair->getFrom()->getTicker(),
            $pair->getTo()->getTicker(),
            $pair->getRate()
        );
    }

    /**
     * @param Pair $pair
     * @throws EntityNotFoundException
     */
    public function remove(Pair $pair): void
    {
        $this->hasCurrencies($pair->getFrom(), $pair->getTo());

        $this->redis->hdel(
            $pair->getFrom()->getTicker(),
            [$pair->getTo()->getTicker()]
        );
    }

    /**
     * @param Currency $from
     * @param Currency $to
     * @throws EntityNotFoundException
     */
    private function hasCurrencies(Currency $from, Currency $to): void
    {
        if (!$this->currencyRepository->has($from) || !$this->currencyRepository->has($to)) {
            throw new EntityNotFoundException(
                sprintf('Currency from "%s" or to "%s" not found', $from->getTicker(), $to->getTicker())
            );
        }
    }
}
