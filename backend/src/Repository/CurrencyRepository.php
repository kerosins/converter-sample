<?php

namespace Converter\Repository;

use Converter\Entity\Currency;
use Predis\Client;

class CurrencyRepository
{
    /**
     * @var Client
     */
    private Client $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @return Currency[]
     */
    public function getAll(): array
    {
        return array_map(
            fn (string $ticker) => new Currency($ticker),
            $this->redis->smembers('currencies')
        );
    }

    /**
     * @param Currency $currency
     */
    public function add(Currency $currency): void
    {
        if ($this->has($currency)) {
            return;
        }

        $this->redis->multi();
        $this->redis->hset($currency->getTicker(), $currency->getTicker(), 1.0);
        $this->redis->sadd('currencies', [$currency->getTicker()]);
        $this->redis->exec();
    }

    /**
     * @param Currency $currency
     * @return bool
     */
    public function has(Currency $currency): bool
    {
        return $this->redis->exists($currency->getTicker());
    }

    /**
     * @param Currency $currency
     */
    public function remove(Currency $currency): void
    {
        if (!$this->has($currency)) {
            return;
        }

        $this->redis->multi();
        $this->redis->del($currency->getTicker());
        $this->redis->srem('currencies', $currency->getTicker());
        $this->redis->exec();
    }
}
