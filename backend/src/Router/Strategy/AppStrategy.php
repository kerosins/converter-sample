<?php

namespace Converter\Router\Strategy;

use Converter\Router\Route;
use Laminas\Diactoros\Response;
use League\Route\Http\Exception\BadRequestException;
use League\Route\Route as LeagueRoute;
use League\Route\Strategy\JsonStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AppStrategy extends JsonStrategy
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     * @param LeagueRoute|Route $route
     */
    public function invokeRouteCallable(LeagueRoute $route, ServerRequestInterface $request): ResponseInterface
    {
        if ($route instanceof Route
            && $route->getRequirements()->hasConstraints()
        ) {
            $errors = $this->validate($route, $request);
            if ($errors) {
                $response = $this->responseFactory->createResponse(400);
                $response->getBody()->write(json_encode($errors));

                return $this->applyDefaultResponseHeaders($response);
            }
        }

        return parent::invokeRouteCallable($route, $request);
    }

    protected function validate(Route $route, ServerRequestInterface $request): array
    {
        $parameters = array_merge(
            $request->getParsedBody(),
            $route->getVars()
        );

        $constraints = $route->getRequirements()->getConstraints();
        $errors = [];
        foreach ($constraints as $parameter => $rules) {
            $violations = $this->validator->validate($parameters[$parameter] ?? null, $constraints[$parameter]);
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[$parameter][] = $violation->getMessage();
            }
        }

        return $errors;
    }
}
