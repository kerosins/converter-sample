<?php

namespace Converter\Router;

use League\Route\Route as BaseRoute;
use League\Route\Router as BaseRouter;

class Router extends BaseRouter
{
    /**
     * Adds new route object
     *
     * @param BaseRoute $route
     */
    public function addRouteObject(BaseRoute $route)
    {
        $this->routes[] = $route;
    }
}
