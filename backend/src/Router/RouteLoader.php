<?php

namespace Converter\Router;

use Converter\Exception\BadConfigException;
use Converter\Router\ValueObject\Requirements;
use Symfony\Component\Yaml\Yaml;

class RouteLoader
{
    /**
     * required keys in route's description
     */
    private const REQUIRED_KEYS = ['method', 'path', 'handler'];

    private Router $router;

    /**
     * Path to yaml with routes
     *
     * @var string
     */
    private string $routesPath;

    /**
     * RouteLoader constructor.
     * @param Router $router
     * @param string $routesPath
     */
    public function __construct(Router $router, string $routesPath)
    {
        $this->router = $router;
        $this->routesPath = $routesPath;
    }

    /**
     * Loads routes
     */
    public function load(): void
    {
        //parse router.yml and add routes
        $routes = Yaml::parseFile($this->routesPath);
        foreach ($routes as $name => $definition) {
            $this->loadRoute($name, $definition);
        }
    }

    /**
     * @param string $name
     * @param array $definition
     */
    public function loadRoute(string $name, array $definition): void
    {
        if (array_diff(self::REQUIRED_KEYS, array_keys($definition))) {
            throw new BadConfigException('Corrupted definition of route');
        }

        $route = new Route($definition['method'], $definition['path'], $definition['handler']);
        $route->setName($name);

        if (isset($definition['requirements']) && is_array($definition['requirements'])) {
            $route->setRequirements(new Requirements($definition['requirements']));
        } else {
            $route->setRequirements(new Requirements());
        }

        $this->router->addRouteObject($route);
    }
}
