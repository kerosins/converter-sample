<?php

namespace Converter\Router\Middleware;

use League\Route\Http\Exception\BadRequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JsonRequestMiddleware implements MiddlewareInterface
{
    /**
     * @inheritdoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (in_array(strtolower($request->getMethod()), ['post', 'put', 'patch', 'delete'])
            && $request->hasHeader('Content-Type')
            && $request->getHeader('Content-Type')[0] === 'application/json'
        ) {
            $parsedBody = json_decode((string) $request->getBody(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new BadRequestException('Invalid json');
            }

            $request = $request->withParsedBody($parsedBody);
        }

        return $handler->handle($request);
    }
}
