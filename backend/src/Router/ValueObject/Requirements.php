<?php

namespace Converter\Router\ValueObject;

use Converter\Exception\BadConfigException;
use Converter\Validator\Constraint\ExistCurrency;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class Requirements
{
    /**
     * array of aliases for constraint classes
     */
    private const CONSTRAINT_ALIASES = [
        'required' => NotBlank::class,
        'existCurrency' => ExistCurrency::class
    ];

    /**
     * @var Constraint[]
     */
    private array $constraints = [];

    /**
     * Requirements constructor.
     * @param array $config Config of constraints
     */
    public function __construct(array $config = [])
    {
        foreach ($config as $parameter => $constraints) {
            $constraints = $this->makeConstraints($constraints ?? []);
            $this->constraints[$parameter] = $constraints;
        }
    }

    /**
     * Creates constraints from definitions
     *
     * @param array $className
     * @param array $options
     */
    private function makeConstraints(array $constraints = []): array
    {
        $result = [];
        foreach ($constraints as $className => $options) {
            $className = isset(self::CONSTRAINT_ALIASES[$className]) ? self::CONSTRAINT_ALIASES[$className] : $className;

            switch ($className) {
                case NotBlank::class:
                    $result[] = new NotBlank([], $options['message'] ?? null);
                    break;

                case ExistCurrency::class:
                    $result[] = new ExistCurrency($options);
                    break;

                default:
                    throw new BadConfigException('Unknown constraint in the route requirements');
            }
        }

        return $result;
    }

    /**
     * @return Constraint[]
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }

    /**
     * Checks if has constrains
     *
     * @return bool
     */
    public function hasConstraints(): bool
    {
        return count($this->constraints) > 0;
    }
}
