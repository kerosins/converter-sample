<?php

namespace Converter\Router;

use Converter\Router\ValueObject\Requirements;
use League\Route\Route as BaseRoute;

class Route extends BaseRoute
{
    private Requirements $requirements;

    /**
     * @return Requirements
     */
    public function getRequirements(): Requirements
    {
        return $this->requirements;
    }

    /**
     * @param Requirements $requirements
     */
    public function setRequirements(Requirements $requirements): void
    {
        $this->requirements = $requirements;
    }
}
