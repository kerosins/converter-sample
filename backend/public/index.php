<?php

use Converter\App;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

require dirname(__DIR__) . '/vendor/autoload.php';

$app = new App();

if ($_SERVER['APP_ENV'] === 'test') {
    $app->enableTestEnvironment();
}
$request = ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$response = $app->processRequest($request);

(new SapiEmitter())->emit($response);